<?php
namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Utility\Text;
use Cake\Validation\Validator;

class ProductPropertiesTable extends Table
{
    public function validationDefault(Validator $validator)
    {
        $validator
            ->notEmpty('title')
            ->minLength('title', 1)
            ->maxLength('title', 255)

            ->notEmpty('value')
            ->minLength('value', 1)
            ->maxLength('value', 255)
        ;
        return $validator;
    }


    public function initialize(array $config)
    {
        $this->addBehavior('Tree', [
            'recoverOrder' => ['sort' => 'DESC'],
        ]);
    }
}
