<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\Controller;

use Cake\Controller\Controller;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{


    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');

        $this->response->cors($this->request)
            ->allowOrigin(['*']) // todo
            ->allowMethods(['GET', 'POST', 'PUT', 'DELETE'])
            ->allowHeaders(['*'])
            ->allowCredentials()
            ->exposeHeaders(['Link'])
            ->maxAge(300)
            ->build();
    }

    public function jsonResponse(int $responseStatusCode = 200,
                                 $responseData = null,
                                 string $responseMessage = '')
    {

        if ($responseStatusCode === 200) {
            // OK
            $status = 'success';
            $data =& $responseData;
            $message = null;
        } else {
            // ERROR
            $status = 'error';
            $data = null;
            $message =& $responseMessage;
        }

        $body = [
            'status' => $status,
            'data' => $data,
            'message' => $message
        ];
        $this->response->type('json');
        $this->response->statusCode($responseStatusCode);
        $this->response->body(json_encode($body));
        $this->response->send();
        $this->render(false, false);

    }
}
