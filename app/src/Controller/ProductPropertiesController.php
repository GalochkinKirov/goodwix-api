<?php
// src/Controller/ArticlesController.php

namespace App\Controller;
use Cake\Network\Exception\BadRequestException;
use Cake\Network\Exception\InternalErrorException;
use Cake\ORM\TableRegistry;

class ProductPropertiesController extends AppController
{

    protected $productPropertiesTable = null;
    protected $ds = null;
    protected $isTransaction = false;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');

        $this->productPropertiesTable = TableRegistry::get('ProductProperties');
        $this->ds = $this->ProductProperties->getConnection();
    }

    public function index()
    {

        $productProperties = $this->ProductProperties->find('threaded')->order(['lft' => 'ASC']);
        $this->jsonResponse(200, $productProperties);
    }

    public function add()
    {
        try {
            $isPost = $this->request->is('post');
            $isJson= $this->request->is('json');
            if (!$isPost || !$isJson) {
                throw new BadRequestException('Invalid input', 400);
            }

            $productPropertiesIncoming = $this->request->input('json_decode', true);
            if (!is_array($productPropertiesIncoming)) {
                throw new BadRequestException('Invalid input (2)', 400);
            }


            $this->ds->begin();
            $this->productPropertiesTable->deleteAll([]);
            $this->isTransaction = true;
            $this->saveMainAndChildrenProductProperties($productPropertiesIncoming);
            $this->ds->commit();

            $productProperties = $this->ProductProperties->find('threaded')->order(['lft' => 'ASC']);
            $this->jsonResponse(200, $productProperties);
        } catch (\Exception $e) {
            if($this->isTransaction) $this->ds->rollback();
            $errorCode = $e->getCode();
            $msg = $e->getMessage();
            $this->jsonResponse($errorCode, null, $msg);
        }
    }

    protected function saveMainAndChildrenProductProperties(array $productPropertiesIncoming)
    {
        foreach ($productPropertiesIncoming as $productPropertyIncoming) {
            $title =& $productPropertyIncoming['title'];
            if (!isset($title)) {
                throw new BadRequestException('Invalid input (3)', 400);
            }

            // Main Product Property
            $productProperty = $this->productPropertiesTable->newEntity();
            $productProperty->title = $title;
            $productProperty->value = '';

            if (!$this->productPropertiesTable->save($productProperty)) {
                throw new InternalErrorException('Internal error (1)', 500);
            }

            $mainId = $productProperty->id;

            $children =& $productPropertyIncoming['children'];
            if (!isset($children) || !is_array($children)) {
                continue;
            }

            $this->saveChildrenProductProperties($children, $mainId);
        }
        return true;
    }

    protected function saveChildrenProductProperties(array $children, int $maiId)
    {
        foreach ($children as $child) {
            $title =& $child['title'];
            $value =& $child['value'];
            if (!isset($title) || !isset($value)) {
                throw new BadRequestException('Invalid input (4)');
            }

            $productProperty = $this->productPropertiesTable->newEntity();
            $productProperty->title = $title;
            $productProperty->value = '';
            $productProperty->parent_id = $maiId;

            if (!$this->productPropertiesTable->save($productProperty)) {
                throw new InternalErrorException('Internal error (2)');
            }
        }
        return true;
    }
}
