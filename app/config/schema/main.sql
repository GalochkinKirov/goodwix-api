BEGIN;

CREATE TABLE product_properties (
    id SERIAL,
    parent_id INT NULL,
    lft INT,
    rght INT,
    title VARCHAR(255),
    value VARCHAR(255),
    PRIMARY KEY(id)
);

COMMIT;
